def bubble_sort(self, numbers):
    
  # loop to access each numbers element
  for i in range(len(numbers)):

    # loop to compare numbers elements
    for j in range(0, len(numbers) - i - 1):

      # compare two adjacent elements
      # change > to < to sort in descending order
      if numbers[j] > numbers[j + 1]:

        # swapping elements if elements
        # are not in the intended order
        temp = numbers[j]
        numbers[j] = numbers[j+1]
        numbers[j+1] = temp
if __name__ == "__main__":
 numbers = list(map(int, input("Enter integer number with space: ")))
 sorted_numbers = bubble_sort(numbers)
 print("Sorted number is", sorted_numbers)